# -*- coding: utf-8 -*-
from django.db.models import QuerySet, Model

from ..dice.models import DiceRoll as DiceRollModel
from ..utils import CostumQuerySetManager, validate_bool


class ActionQuerySet(QuerySet):
    def roll_for_action(self, action: Model, **kwargs) -> Model:
        if not action.is_concluded:
            action.roll = DiceRollModel.objects.create(**kwargs)
            action.save()
        else:
            action.roll.re_roll(**kwargs)
        return action


class ActionManager(CostumQuerySetManager):
    def get_queryset(self) -> QuerySet:
        """
        Retreive costum QuerySet class (ActionQuerySet).
        :return :type QuerySet
        """
        return ActionQuerySet(self.model, using=self._db)

    def get_by_id_and_roll(self, id: int, **kwargs) -> Model:
        return self.get_queryset().roll_for_action(
            action=self.get(id=id), **kwargs
        )

    def create_and_roll(self, instruction: str, **kwargs) -> Model:
        return self.get_queryset().roll_for_action(
            action=self.create(**kwargs), instruction=instruction
        )

    def get_actions(self, concluded=None) -> QuerySet:
        if concluded is None:
            return self.all()
        validate_bool(value=concluded, name="concluded")
        return self.filter(roll__isnull=(not concluded))
