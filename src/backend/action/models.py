# -*- coding: utf-8 -*-
from typing import Tuple

from django.db import models
from django.utils.translation import gettext_lazy as _

from . import DEFUALT
from .manager import ActionManager
from ..dice.models import DiceRoll
from ..utils import TimeStamp


class Action(TimeStamp):
    action = models.CharField(
        max_length=100,
        default=DEFUALT,
        help_text=_("Act/move you wish to make"),
    )
    target = models.CharField(
        max_length=100,
        default=DEFUALT,
        help_text=_("Subject to execute action on"),
    )
    roll = models.OneToOneField(
        to=DiceRoll,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("Dice roll to fulfill action"),
    )
    target_roll_value = models.IntegerField(
        default=0,
        help_text=_(
            "Minimum value to roll in order to successfully execute action on target"
        ),
    )

    objects = ActionManager()

    @property
    def success(self) -> bool:
        return (
            self.roll.value >= self.target_roll_value
            if self.is_concluded
            else False
        )

    @property
    def is_concluded(self) -> bool:
        return self.roll is not None

    class Meta:
        ordering: Tuple = ("created",)

    def __str__(self) -> str:
        return f"{self.id}: {self.action}"
