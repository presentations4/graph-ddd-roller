# -*- coding: utf-8 -*-
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DiceConfig(AppConfig):
    name = "src.backend.dice"
    verbose_name = _("Dice")
