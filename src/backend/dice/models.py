# -*- coding: utf-8 -*-
from json import dumps, loads
from typing import Tuple, List

from django.db import models
from django.utils.translation import gettext_lazy as _
from dddroller.utilities import cherrypick_roll, pick_instruction_parser
from dddroller.funtion_roller import dice_roll

from .manager import DiceRollManager
from ..utils import TimeStamp


class DiceRoll(TimeStamp):
    instruction = models.CharField(
        max_length=100, help_text=_("Dice roll instruction command")
    )
    _rolls = models.CharField(max_length=255)
    modifier = models.IntegerField(
        default=0,
        help_text=_("Value to modify the sum of picked rolls"),
    )

    objects = DiceRollManager()

    @property
    def picks(self) -> Tuple:
        return cherrypick_roll(
            rolls=self.rolls,
            **pick_instruction_parser(instruction=self.instruction),
        )

    @property
    def rolls(self):
        return loads(self._rolls)

    @property
    def value(self) -> int:
        return sum(self.picks) + self.modifier

    @property
    def is_orphan(self) -> bool:
        return self.action is None

    def re_roll(self, instruction=None) -> "DiceRoll":
        dice = dice_roll(instruction=instruction or self.instruction)
        if instruction and instruction != self.instruction:
            self.instruction = dice.instruction
            self.modifier = dice.modifier

        self._rolls = dumps(dice.rolls)
        self.save()
        return self

    class Meta:
        ordering: Tuple = ("created", "modified")

    def __str__(self) -> str:
        return f"{self.id}: {self.instruction}"
