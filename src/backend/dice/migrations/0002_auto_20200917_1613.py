# -*- coding: utf-8 -*-
# Generated by Django 2.2.16 on 2020-09-17 16:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dice', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diceroll',
            name='instruction',
            field=models.CharField(help_text='Dice roll instruction command', max_length=100),
        ),
        migrations.AlterField(
            model_name='diceroll',
            name='modifier',
            field=models.IntegerField(default=0, help_text='value that will be added to your final roll total'),
        ),
    ]
