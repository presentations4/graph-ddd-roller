# -*- coding: utf-8 -*-
from json import dumps

from django.db.models import QuerySet, Model
from dddroller.funtion_roller import dice_roll, DiceRoll

from ..utils import CostumQuerySetManager, validate_bool


class DiceRollQuerySet(QuerySet):
    def create(self, **kwargs) -> Model:
        roll = dice_roll(**kwargs)
        return super().create(
            instruction=roll.instruction,
            modifier=roll.modifier,
            _rolls=dumps(roll.rolls),
        )


class DiceRollManager(CostumQuerySetManager):
    def get_dice_rolls(self, orphan=None) -> QuerySet:
        if orphan is None:
            return self.get_queryset().all()
        validate_bool(value=orphan, name="orphan")
        return self.get_queryset().filter(action__isnull=orphan)

    def get_queryset(self) -> QuerySet:
        """
        Retreive costum QuerySet class (DiceRollQuerySet).
        :return :type QuerySet
        """
        return DiceRollQuerySet(self.model, using=self._db)
