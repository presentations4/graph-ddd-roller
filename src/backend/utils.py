# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from typing import Any

from django.db.models import Manager, QuerySet, Model, DateTimeField
from django.utils.translation import gettext_lazy as _


class TimeStamp(Model):
    created = DateTimeField(
        auto_now_add=True, help_text=_("Timestamp of this datapoint's creation")
    )
    modified = DateTimeField(
        auto_now=True,
        help_text=_("Timestamp of this datapoint's last modification"),
    )

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}({self.__str__()!r})>"

    class Meta:
        abstract = True


class CostumQuerySetManager(Manager, ABC):
    def __getattr__(self, attr, *args):
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            # don't delegate internal methods to the queryset
            if attr.startswith("__") and attr.endswith("__"):
                raise
            return getattr(self.get_query_set(), attr, *args)

    @abstractmethod
    def get_queryset(self) -> QuerySet:
        pass


def validate_bool(value: Any, name: str) -> Any:
    if not isinstance(value, bool):
        raise ValueError(f"{name} of type {type(value)} is not a bool")
    return value
