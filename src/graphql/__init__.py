# -*- coding: utf-8 -*-
import graphene

from .queries.action import ActionQuery
from .queries.dice import DiceRollQuery
from .mutations.action import ActionMutation
from .mutations.dice import DiceRollMutation


class Query(
    DiceRollQuery,
    ActionQuery,
):
    """Project wide registeration for query"""

    pass


class Mutation(
    graphene.ObjectType,
    DiceRollMutation,
    ActionMutation,
):
    """Project wide registeration for mutation"""

    pass


schema: graphene.Schema = graphene.Schema(query=Query, mutation=Mutation)
