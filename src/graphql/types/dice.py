# -*- coding: utf-8 -*-
from django.db.models import Model
from django.utils.translation import gettext_lazy as _
from graphene import Int, List, NonNull, Boolean, String, ObjectType
from graphene_django import DjangoObjectType

from src.backend.dice.models import DiceRoll as DiceRollModel


class CoreDiceRollFields:
    rolls = List(NonNull(Int), description=_("Integer list all rolls"))
    picks = List(
        NonNull(Int),
        description=_("Ordered integer list of your prefered rolls"),
    )
    value = Int(
        description=_("Total value of your roll: picked rolls + modifier"),
        required=True,
    )


class DiceRoll(CoreDiceRollFields, DjangoObjectType):
    """Set of dice roll values."""

    class Meta:
        model: Model = DiceRollModel
        exclude: tuple = ("_rolls",)


class VolitileDiceRoll(CoreDiceRollFields, ObjectType):
    """None Recorded set of dice roll values."""

    modifier = Int(
        required=True, description=_("Value to modify the sum of picked rolls")
    )
    instruction = String(
        required=True, description=_("Dice roll instruction command.")
    )
