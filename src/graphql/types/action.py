# -*- coding: utf-8 -*-
from django.db.models import Model
from django.utils.translation import gettext_lazy as _
from graphene import Boolean
from graphene.relay import Node
from graphene_django import DjangoObjectType

from src.backend.action.models import Action as ActionModel


class Action(DjangoObjectType):
    success = Boolean(
        description=_(
            "Dice roll has met or exceded minimum trash hold for action execution"
        )
    )
    is_concluded = Boolean(
        description=_("Dice roll already preformed for this action")
    )

    class Meta:
        model = ActionModel
        filter_fields = ["success", "is_concluded"]
        interface = (Node,)
