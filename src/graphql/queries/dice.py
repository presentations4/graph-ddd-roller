# -*- coding: utf-8 -*-
from django.utils.translation import gettext_lazy as _
from graphene import ObjectType, String, Int, Boolean, ResolveInfo, Field, List
from dddroller.funtion_roller import dice_roll, DiceRoll

from src.backend.dice.models import DiceRoll as DiceRollModel

from ..types.dice import (
    DiceRoll as DiceRollType,
    VolitileDiceRoll as VolitileDiceRollType,
)


def roll(**kwargs):
    """execute dice roll instruction and return iets result as a DiceRoll

    :param instruction: one line string instruction, defaults to None
    :type instruction: str, Optional
    :return: dice roll result
    :rtype: DiceRoll
    """
    _roll: DiceRoll = dice_roll(**kwargs)
    return DiceRollType(
        instruction=_roll.instruction,
        rolls=_roll.rolls,
        picks=_roll.picks,
        modifier=_roll.modifier,
        value=_roll.value,
    )


class DiceRollQuery(ObjectType):
    """All Query operations that has to do with RecordedDiceRoll object

    :param ObjectType: The building block used to define the relationship in your Schema
    :type ObjectType: graphene.types.objecttype.ObjectType
    """

    dice_rolls = List(
        DiceRollType,
        description=_(
            "Filter through orphan dice rolls. !!No argument will retreive the union (all dice rolls)!!"
        ),
        orphan=Boolean(
            description=_("Recorded dice rolls with no related actions")
        ),
    )

    value_instruction_roll = Field(
        VolitileDiceRollType,
        deprecation_reason=_(
            "Moving forward with string diceroll commands. Use stringInstructionRoll instead"
        ),
        required=True,
        description=_("Execute dice roll by instruction values"),
        die=Int(required=True, description=_("Nth sided die you wish to roll")),
        quantity=Int(description=_("How many die you wish to roll")),
        decending=Boolean(description=_("Dice roll picking order by value")),
        pick=Int(description=_("How many rolled dice to count")),
        modifier=Int(description=_("number to modify the sum rolls with")),
    )
    string_instruction_roll = Field(
        VolitileDiceRollType,
        required=True,
        description=_("Execute dice roll by instruction command string"),
        instruction=String(
            required=True,
            description=_(
                "<dice quantity><nth sided dice><([b]est|[w]orst) pick><modifier> example: 4d6w3+1"
            ),
        ),
    )

    def resolve_dice_rolls(self, info, **kwargs):
        return DiceRollModel.objects.get_dice_rolls(**kwargs)

    def resolve_value_instruction_roll(
        self,
        info: ResolveInfo,
        die: int,
        decending: bool = True,
        **kwargs,
    ) -> DiceRollType:
        return roll(die=die, best=decending, **kwargs)

    def resolve_string_instruction_roll(
        root, info, instruction, **kwargs
    ) -> DiceRollType:
        return roll(instruction=instruction)
