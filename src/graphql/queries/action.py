# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _
from graphene import ObjectType, String, Int, Boolean, ResolveInfo, Field, List

from src.backend.action.models import Action as ActionModel

from ..types.action import Action as ActionType


class ActionQuery(ObjectType):
    actions = List(
        ActionType,
        concluded=Boolean(
            description=_("Action already holds a dice roll value")
        ),
        description=_(
            "Filter through actions by its conclusion. !!No argument will retreive the union (all actions)!!"
        ),
    )

    action_by_id = Field(
        ActionType,
        id_=Int(required=True, description=_("Action id")),
        description=_("Obtain an action by id"),
    )

    def resolve_action_by_id(self, info: ResolveInfo, id_: int, **kwargs):
        try:
            return ActionModel.objects.get_action_by_id(id_=id_)
        except ObjectDoesNotExist:
            pass

    def resolve_actions(self, info: ResolveInfo, **kwargs):
        return ActionModel.objects.get_actions(**kwargs)
