# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _
from graphene import Mutation, String, ResolveInfo, Field, Int

from src.backend.dice.models import DiceRoll as DiceRollModel

from ..types.dice import DiceRoll as DiceRollType


class RecordDiceRoll(Mutation):
    """
    Record your dice roll to the database.

    request is similar to stringInstructionRoll query operation,
    but with the added feature of saving your dice roll to the database.
    """

    class Arguments:
        instruction = String(
            required=True, description=_("Dice roll instruction command")
        )

    dice_roll = Field(DiceRollType)

    def mutate(self, info: ResolveInfo, instruction: str) -> "RecordDiceRoll":
        return RecordDiceRoll(
            dice_roll=DiceRollModel.objects.create(instruction=instruction)
        )


class ReRoll(Mutation):
    """
    Reroll your dice roll for an already recorded dice roll.

    using the same dice roll instruction.
    """

    class Arguments:
        id = Int(required=True, description=_("Id of the dice roll to re-roll"))

    dice_roll = Field(DiceRollType)

    def mutate(self, info: ResolveInfo, id: int) -> "ReRoll":
        try:
            return ReRoll(dice_roll=DiceRollModel.objects.get(id=id).re_roll())
        except ObjectDoesNotExist:
            pass


class DiceRollMutation:
    record_dice_roll = RecordDiceRoll.Field()
