# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _
from graphene import (
    Mutation,
    String,
    Int,
    Field,
    ResolveInfo,
)

from src.backend.action.models import Action as ActionModel

from ..types.action import Action as ActionType


class RollForActionByID(Mutation):
    """
    Given an Action id.
    if action object exists and has yet to be rolled,
    then execute a dice roll for this action.
    """

    class Arguments:
        id = Int(required=True, description=_("Action id"))
        instruction = String(description=_("dice roll instruction"))

    action = Field(ActionType)

    def mutate(self, info: ResolveInfo, id: int, **kwargs):
        try:
            return RollForActionByID(
                action=ActionModel.objects.get_by_id_and_roll(id=id, **kwargs)
            )
        except ObjectDoesNotExist:
            pass


class ActionArguments:
    action = String()
    target = String()
    target_roll_value = Int()


class CreateAction(Mutation):
    class Arguments(ActionArguments):
        pass

    action = Field(ActionType)

    def mutate(self, info: ResolveInfo, **kwargs):
        return CreateAction(action=ActionModel.objects.create(**kwargs))


class CreateActionAndRoll(Mutation):
    class Arguments(ActionArguments):
        instruction = String(
            required=True, description=_("dice roll instruction")
        )

    action = Field(ActionType)

    def mutate(self, info: ResolveInfo, instruction: str, **kwargs):
        return CreateAction(
            action=ActionModel.objects.create_and_roll(
                instruction=instruction, **kwargs
            )
        )


class ActionMutation:
    roll_for_action_by_id = RollForActionByID.Field()
    create_action = CreateAction.Field()
    create_action_and_roll = CreateActionAndRoll.Field()
