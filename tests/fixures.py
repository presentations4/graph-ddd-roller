# -*- coding: utf-8 -*-
import pytest
from graphene_django.utils.testing import graphql_query, Client
from .factory import ActionFactory, DiceRollFactory


@pytest.fixture
def client_query(client):
    def func(*args, **kwargs):
        return graphql_query(*args, **kwargs, client=client)

    return func


@pytest.fixture
def action(db):
    return ActionFactory()


@pytest.fixture
def dice_roll(db):
    return DiceRollFactory()


@pytest.fixture
def concluded_action(db):
    action = ActionFactory()
    action.roll = DiceRollFactory()
    action.save()
    return action
