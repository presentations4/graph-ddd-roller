# -*- coding: utf-8 -*-
from manage import __version__


def test_version():
    assert __version__ == "0.2.0"
