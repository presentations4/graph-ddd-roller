# -*- coding: utf-8 -*-
from factory.django import DjangoModelFactory


class DiceRollFactory(DjangoModelFactory):
    class Meta:
        model = "dice.DiceRoll"

    instruction = "4d12b3"


class ActionFactory(DjangoModelFactory):
    class Meta:
        model = "action.Action"
