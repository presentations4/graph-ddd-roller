# -*- coding: utf-8 -*-
from collections import OrderedDict
from datetime import date, datetime
from json import loads

import pytest

from src.backend.action import DEFUALT, BORN, CHARACTER

from ..fixures import client_query, action

ACTION: int = 0
TARGET: int = 1
ROLL: int = 2
TARGET_VALUE: int = 3
SUCCESS: int = 4
CONCLUDED: int = 5


@pytest.mark.parametrize(
    "arguments, values",
    [
        ("", (DEFUALT, DEFUALT, None, 0)),
        (f'(action: "{BORN}")', (BORN, DEFUALT, None, 0)),
        (
            f'(action: "{BORN}", target: "{CHARACTER}")',
            (BORN, CHARACTER, None, 0),
        ),
        (
            f'(action: "{BORN}", target: "{CHARACTER}", targetRollValue: 5)',
            (BORN, CHARACTER, None, 5),
        ),
    ],
)
def test_create_action(
    arguments, values, client_query, db  # noqa: F811
) -> None:
    responds = client_query(
        f"""
mutation {'{'}
    createAction{arguments} {'{'}
        action{'{'}
            action
            target
            roll{'{'}
                instruction
            {'}'}
            targetRollValue
            success
            isConcluded
        {'}'}
    {'}'}
{'}'}""",
    )
    assert loads(responds.content) == {
        "data": OrderedDict(
            [
                (
                    "createAction",
                    {
                        "action": {
                            "action": values[ACTION],
                            "target": values[TARGET],
                            "roll": values[ROLL],
                            "targetRollValue": values[TARGET_VALUE],
                            "success": False,
                            "isConcluded": False,
                        }
                    },
                )
            ]
        )
    }


@pytest.mark.parametrize(
    "arguments, values",
    [
        ('(instruction: "d6")', (DEFUALT, DEFUALT, "d6", 0, True, True)),
        (
            f'(action: "{BORN}", instruction: "d6")',
            (BORN, DEFUALT, "d6", 0, True, True),
        ),
        (
            f'(action: "{BORN}", target: "{CHARACTER}", instruction: "d6")',
            (BORN, CHARACTER, "d6", 0, True, True),
        ),
        (
            f'(action: "{BORN}", target: "{CHARACTER}", targetRollValue: 7, instruction: "d6")',
            (BORN, CHARACTER, "d6", 7, False, True),
        ),
    ],
)
def test_create_action_and_roll(
    arguments, values, client_query, db  # noqa: F811
) -> None:
    responds = client_query(
        f"""
mutation {'{'}
    createActionAndRoll{arguments} {'{'}
        action{'{'}
            action
            target
            roll{'{'}
                instruction
            {'}'}
            targetRollValue
            success
            isConcluded
        {'}'}
    {'}'}
{'}'}""",
    )
    assert loads(responds.content) == {
        "data": OrderedDict(
            [
                (
                    "createActionAndRoll",
                    {
                        "action": {
                            "action": values[ACTION],
                            "target": values[TARGET],
                            "roll": {"instruction": values[ROLL]},
                            "targetRollValue": values[TARGET_VALUE],
                            "success": values[SUCCESS],
                            "isConcluded": values[CONCLUDED],
                        }
                    },
                )
            ]
        )
    }


def test_roll_for_action_by_id(action, client_query, db):  # noqa: F811
    instruction = "4d10b3"
    assert not action.is_concluded
    responds = client_query(
        f"""
mutation {'{'}
    rollForActionById(instruction: "{instruction}", id: {action.id}){'{'}
        action{'{'}
            action
            target
            roll{'{'}
                instruction
            {'}'}
            targetRollValue
            success
            isConcluded
        {'}'}
    {'}'}
{'}'}""",
    )
    assert loads(responds.content) == {
        "data": OrderedDict(
            [
                (
                    "rollForActionById",
                    {
                        "action": {
                            "action": DEFUALT,
                            "target": DEFUALT,
                            "roll": {"instruction": instruction},
                            "targetRollValue": 0,
                            "success": True,
                            "isConcluded": True,
                        }
                    },
                )
            ]
        )
    }
