# -*- coding: utf-8 -*-
import pytest
from json import loads

from ..fixures import client_query


@pytest.mark.parametrize(
    "value, modifier",
    [
        ("2d10+5", 5),
        ("d10-3", -3),
        ("4d6b3+1", 1),
        ("6d8w3-2", -2),
        ("1d6-3", -3),
        ("d6", 0),
        ("d100", 0),
        ("10d8-5", -5),
    ],
)
def test_string_instruction_roll(
    value, modifier, client_query, db  # noqa: F811
) -> None:
    responds = client_query(
        f"""{"{"}
    stringInstructionRoll(instruction: "{value}"){"{"}
        instruction
        modifier
    {"}"}
{"}"}""",
    )
    assert loads(responds.content) == {
        "data": {
            "stringInstructionRoll": {
                "instruction": value,
                "modifier": modifier,
            }
        }
    }


@pytest.mark.parametrize(
    "die, quantity, pick, decending, instruction, modifier",
    [
        (10, 2, 0, "true", "2d10+5", 5),
        (10, 1, 1, "true", "d10-3", -3),
        (6, 4, 3, "true", "4d6B3+1", 1),
        (8, 6, 3, "false", "6d8W3-2", -2),
        (6, 1, 0, "false", "d6-3", -3),
    ],
)
def test_value_instruction_roll(
    die,  # noqa: F811
    quantity,  # noqa: F811
    pick,  # noqa: F811
    decending,  # noqa: F811
    instruction,  # noqa: F811
    modifier,  # noqa: F811
    client_query,  # noqa: F811
    db,  # noqa: F811
) -> None:
    responds = client_query(
        f"""{"{"}
    valueInstructionRoll(die: {die}, quantity: {quantity}, pick: {pick} decending: {decending}, modifier: {modifier}){"{"}
        instruction
        modifier
    {"}"}
{"}"}""",
    )
    assert loads(responds.content) == {
        "data": {
            "valueInstructionRoll": {
                "instruction": instruction,
                "modifier": modifier,
            }
        }
    }


@pytest.mark.parametrize(
    "arguments, instruction",
    [
        ("die: 6, quantity: 3, modifier: -2", "3d6-2"),
        ("die: 6", "d6"),
        ("die: 6, quantity: 3, decending: false, pick: 1", "3d6W1"),
    ],
)
def test_value_instruction_roll_arguments(
    arguments, instruction, client_query, db  # noqa: F811
) -> None:
    responds = client_query(
        f"""{"{"}
    valueInstructionRoll({arguments}){"{"}
        instruction
    {"}"}
{"}"}""",
    )
    assert loads(responds.content) == {
        "data": {"valueInstructionRoll": {"instruction": instruction}}
    }
