# -*- coding: utf-8 -*-
import pytest

from src.backend.action.models import Action as ActionModel
from ..fixures import action, dice_roll, concluded_action


def test_create_action_default_values(action) -> None:  # noqa: F811
    assert action.action == "Nothing"
    assert action.target == "Nothing"
    assert action.roll is None
    assert action.target_roll_value == 0


def test_create_action_property_values(action, dice_roll) -> None:  # noqa: F811
    assert not action.is_concluded
    assert not action.success
    action.roll = dice_roll
    assert action.is_concluded
    assert action.success


def test_roll_for_action_by_id(action) -> None:  # noqa: F811
    assert not action.is_concluded
    instruction = "4d6b3"
    updated_action = ActionModel.objects.get_by_id_and_roll(
        action.id, instruction=instruction
    )
    assert updated_action.is_concluded
    assert updated_action.roll.instruction == instruction


def test_roll_for_concluded_action_by_id(
    concluded_action,  # noqa: F811
) -> None:
    assert concluded_action.is_concluded
    instruction = "4d6b3"
    assert concluded_action.roll.instruction != instruction
    updated_action = ActionModel.objects.get_by_id_and_roll(
        concluded_action.id, instruction=instruction
    )
    assert updated_action.is_concluded
    assert updated_action.roll.instruction == instruction


def test_create_and_roll(db) -> None:  # noqa: F811
    instruction = "4d6b3"
    _action = ActionModel.objects.create_and_roll(instruction=instruction)
    assert _action.action == "Nothing"
    assert _action.target == "Nothing"
    assert _action.is_concluded
    assert _action.roll.instruction == instruction
    assert _action == ActionModel.objects.get(id=_action.id)


def test_get_actions(action, concluded_action) -> None:  # noqa: F811
    assert len(ActionModel.objects.get_actions()) == 2
    concluded = ActionModel.objects.get_actions(concluded=True)
    assert len(concluded) == 1
    assert concluded.first() == concluded_action
    not_concluded = ActionModel.objects.get_actions(concluded=False)
    assert len(not_concluded) == 1
    assert not_concluded.first().id == action.id


def test_get_actions_not_bool() -> None:  # noqa: F811
    with pytest.raises(ValueError):
        ActionModel.objects.get_actions(concluded="string")
