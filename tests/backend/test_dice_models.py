# -*- coding: utf-8 -*-
import pytest

from src.backend.dice.models import DiceRoll as DiceRollModel

from ..fixures import dice_roll, concluded_action
from ..factory import DiceRollFactory


def test_create_dice_roll_no_input(db) -> None:
    with pytest.raises(AttributeError):
        DiceRollModel.objects.create()


def test_create_dice_roll_property_values(dice_roll) -> None:  # noqa: F811
    assert isinstance(dice_roll.picks, tuple)
    assert len(dice_roll.picks) == 3
    assert isinstance(dice_roll.rolls, list)
    assert len(dice_roll.rolls) == 4
    assert 0 < dice_roll.value <= 36


def test_create_dice_roll(dice_roll) -> None:  # noqa: F811
    assert dice_roll.instruction == "4d12b3"
    assert isinstance(dice_roll._rolls, str)
    assert dice_roll.modifier == 0  # default


def test_create_multiple_dice_roll_same_instruction(
    dice_roll,  # noqa: F811
) -> None:
    another_dice_roll = DiceRollFactory()
    assert dice_roll.id != another_dice_roll.id
    assert dice_roll.instruction == another_dice_roll.instruction


def test_get_dice_rolls(dice_roll, concluded_action) -> None:  # noqa: F811
    assert len(DiceRollModel.objects.get_dice_rolls()) == 2
    orphans = DiceRollModel.objects.get_dice_rolls(orphan=True)
    assert len(orphans) == 1
    assert orphans.first() == dice_roll
    not_orphans = DiceRollModel.objects.get_dice_rolls(orphan=False)
    assert len(not_orphans) == 1
    assert not_orphans.first() == concluded_action.roll


def test_re_roll(db) -> None:  # noqa: F811
    rolls = DiceRollFactory(instruction="25d20+3")
    initial_rolls = rolls.rolls
    assert rolls.re_roll().rolls != initial_rolls


def test_re_roll_new_instruction(dice_roll) -> None:  # noqa: F811
    instruction: str = "5d10w3+2"
    rolls_len = len(dice_roll.rolls)
    modifier = dice_roll.modifier
    assert dice_roll.instruction != instruction
    dice_roll.re_roll(instruction=instruction)
    assert dice_roll.instruction == instruction
    assert len(dice_roll.rolls) != rolls_len
    assert dice_roll.modifier != modifier


def test_get_actions_not_bool() -> None:  # noqa: F811
    with pytest.raises(ValueError):
        DiceRollModel.objects.get_dice_rolls(orphan="string")
