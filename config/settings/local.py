# -*- coding: utf-8 -*-
from environ import Env

from .base import *  # noqa
from .base import t, INSTALLED_APPS, MIDDLEWARE, StringKeyDict
from .. import DEBUG_TOOLBAR

env = Env()

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY: str = env.str("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS: t.List[str] = ["localhost", "127.0.0.1"]

INTERNAL_IPS = ["127.0.0.1"]

INSTALLED_APPS += DEBUG_TOOLBAR

MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES: StringKeyDict = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "USER": env.str("POSTGRES_USER"),
        "PASSWORD": env.str("POSTGRES_PASSWORD"),
        "HOST": env.str("POSTGRES_HOST"),
        "PORT": env.int("POSTGRES_PORT"),
        "NAME": env.str("POSTGRES_DB"),
    }
}
