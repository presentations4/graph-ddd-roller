# -*- coding: utf-8 -*-
from environ import Env

from .base import *  # noqa
from .base import t, INSTALLED_APPS, MIDDLEWARE, StringKeyDict
from .. import DEBUG_TOOLBAR


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY: str = "TeStInG-kEy"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS: t.List[str] = ["localhost", "127.0.0.1"]

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
env = Env()
DATABASES: StringKeyDict = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env.str("POSTGRES_DB"),
        "HOST": "Postgres",
        "USER": env.str("POSTGRES_USER"),
        "PASSWORD": env.str("POSTGRES_PASSWORD"),
    }
}
