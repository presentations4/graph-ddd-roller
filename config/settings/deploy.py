# -*- coding: utf-8 -*-
import django_heroku
from environ import Env

from .base import *  # noqa

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = bool(Env().int("DEBUG"))

TEST_RUNNER: str = "django_heroku.HerokuDiscoverRunner"

django_heroku.settings(locals())
