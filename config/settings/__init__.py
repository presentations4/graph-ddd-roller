# -*- coding: utf-8 -*-
DJANGO_EXT: str = "django_extensions"
GRAPHQL: str = "graphene_django"
DJANGO_FILTER: str = "django_filter"
